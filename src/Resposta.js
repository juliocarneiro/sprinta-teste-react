import React, { Component } from 'react';

class Resposta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ""
        }
    }
    handleChange = (event) => {
        this.setState({text: event.target.value});
    }
    render() {
        const { show } = this.state;
        return (
        <div className="resposta-wrap">
            <button onClick={() => this.setState({ show: !show })} style={{ display: (show ? 'none' : 'inline-block') }}>Responder</button>
            <form className='resposta' style={{ display: (show ? 'inline-block' : 'none') }} action={this.props.action}>
                <h4>Name</h4>
                <textarea onChange={this.handleChange}></textarea>
                <input type="button" value="Voltar" onClick={() => this.setState({ show: !show })} />
                <input type="submit" disabled={this.state.text.length === 0} value="Publicar Comentário" />
            </form>
        </div>
        )
    }
}
  
export default Resposta;