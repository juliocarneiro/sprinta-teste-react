import React, { Component } from 'react';
import Resposta from './Resposta';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="comentario">
          <h4>Marcelo do Amaral</h4>
          <span>29/09/2017 - 21:32</span>
          <p>Lorem ipsum dolor amat</p>
          <Resposta action="/post" />
        </div>
      </div>
    );
  }
}

export default App;
